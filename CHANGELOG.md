# Changelog

## 1.1.2

- Updated typo in CHANGELOG
- Updated README (Gebruik and Compatibiliteit sections)

## 1.1.1

- Updated ```$quickedit-font-path``` setting so vendor fonts aren't mixed with application fonts

## 1.1.0

- First minor release

## 1.0.6

- Added static example for testing purposes, located in dist dir

## 1.0.5

- Removed checkboxes from README since this isn't supported on BitBucket

## 1.0.4

- Added TODO section to README

## 1.0.3

- Added some reset style rules
- Added box-sizing
- Position buttons on < 600px
- General style updates

## 1.0.2

- Fixed undefined vars

## 1.0.1

- Added missing var

## 1.0.0

- Initial release
