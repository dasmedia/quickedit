# Quickedit

Via de **Quickedit** tool kan men via een dialog window vertalingen gaan wijzigen vanaf de frontend. Vooraleer men kan gebruik maken van deze functionaliteit moet de gebruiker eerst inloggen in het beheersysteem, vervolgens zal de tool beschikbaar zijn op de frontend.

## Installatie

```
$ bower install --save quickedit=https://dasmedia@bitbucket.org/dasmedia/quickedit.git
```

### Updaten

```
$ bower update quickedit
```
Build public assets via ```$ grunt dev``` of ```$ grunt production```

## Gebruik

* Voeg referentie toe naar quickedit.js aan de **concat** task in **Gruntfile.js**

``` javascript
'<%= app.resources %>/bower_components/quickedit/src/scripts/quickedit.js',
```

* **Bowercopy** task uitbreiden in **Gruntfile.js** om quickedit font te kopiëren naar public directory

``` javascript
fonts: {
                options: {
                    destPrefix: '<%= app.public %>/fonts/',
                },
                files: {
                    'vendor': 'quickedit/src/fonts/**/*.{eot,woff,woff2,ttf,svg}',
                },
            },
```

* Maak een nieuwe vendor file (*_vendors.quickedit.scss*) aan in resources/styles/ en importeer de quickedit vendor styles

``` scss
@import '../../bower_components/quickedit/src/styles/quickedit.scss';
```

* Importeer *_vendors.quickedit.scss* in **main.scss** en **checkout.scss** master files
* Build public assets via ```$ grunt dev``` of ```$ grunt production```

### Compatibiliteit

* Verwijder referentie naar inline-edit.js (concat task) in Gruntfile.js
* Verwijder inline-edit.js source file in /resources/scripts/vendor
* Verwijder alle 'quickedit' referenties (imports, component, settings ...) in /resources/styles
* Verwijder custom ```dfn``` layout in /resources/styles (import, component, ...)
* [Toevoegen extra tasks grunt watch task ](https://bitbucket.org/dasmedia/das-app/commits/d57e94f07b8176980c9f18c91af0a79b16682ef3)
* Installeer en volg de instructies bij **Gebruik** om de quickedit component als vendor in het project te implementeren

### Default SASS settings

``` scss
$quickedit-dfn-border-color:               $manatee !default;
$quickedit-text-color:                     $shark !default;
$quickedit-border-color:                   $gallery !default;
$quickedit-background-color:               $gallery !default;
$quickedit-error-color:                    $cinnabar !default;

$quickedit-button-cancel-text-color:       $white !default;
$quickedit-button-cancel-bg-color:         $carnation !default;
$quickedit-button-cancel-hover-text-color: $white !default;
$quickedit-button-cancel-hover-bg-color:   darken($quickedit-button-cancel-bg-color, 5%) !default;

$quickedit-button-save-text-color:         $white !default;
$quickedit-button-save-bg-color:           $sushi !default;
$quickedit-button-save-hover-text-color:   $white !default;
$quickedit-button-save-hover-bg-color:     darken($quickedit-button-save-bg-color, 5%) !default;

$quickedit-border-radius:                  3px !default;
$quickedit-input-padding:                  15px 20px !default;

$quickedit-font-size:                      12px !default;
$quickedit-font-family:                    'Verdana', 'Arial', sans-serif !default;

$quickedit-icon-font-size:                 18px !default;

$quickedit-font-path:                      '../fonts/vendor' !default;

$quickedit-transition:                     all .2s ease !default;
```

### Settings overschrijven

Default settings kunnen overschreven worden door de variabele opnieuw te declareren net voor de import regel.

*Voorbeeld:*

``` scss
$quickedit-background-color: #efefef;

@import '../../bower_components/quickedit/src/styles/quickedit.scss';
```

## Ontwikkelen

TODO: write contributing instructions, hoe te builden etc, dependecies installeren, grunt tasks, ...

## Changelog

Bekijk de [Changelog](CHANGELOG.md).