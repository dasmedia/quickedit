module.exports = function(grunt) {

    // Execute function in strict mode
    'use strict';

    // Automatically load required grunt tasks
    require('jit-grunt')(grunt, {
        scsslint: 'grunt-scss-lint',
    });

    // Display the elapsed execution time of grunt tasks
    require('time-grunt')(grunt);

    /**
     * Configurable paths
     */

    var paths = {
        resources: 'src',
        public: 'dist',
        views: 'View',
        plugins: 'Plugin',
    };

    /**
     * Define Grunt tasks
     */

    grunt.initConfig({

        // get package name from manifest
        pkg: grunt.file.readJSON('package.json'),

        // Define asset paths
        app: paths,

        // Watch assets
        watch: {
            styles: {
                files: ['<%= app.resources %>/styles/**/*.scss'],
                tasks: ['sass:dev']
            },
            scripts: {
                files: '<%= app.resources %>/scripts/**/*.js',
                tasks: ['concat:main'],
            },
            fonts: {
                files: ['<%= app.resources %>/fonts/**/*.{eot,woff,woff2,ttf,svg}'],
                tasks: ['sync:fonts']
            },
            icons: {
                files: ['<%= app.resources %>/icons/**/*.svg'],
                tasks: ['webfont']
            },
            configFiles: {
                files: ['Gruntfile.js'],
                options: {
                    reload: true
                }
            }
        },

        // BrowserSync
        browserSync: {
            target: {
                bsFiles: {
                    src: [
                        '<%= app.public %>/css/**/*.css',
                        '<%= app.public %>/js/**/*.js',
                        '<%= app.public %>/**/*.html',
                    ]
                },
                options: {
                    server: {
                        baseDir: '<%= app.public %>',
                        index: 'example.html',
                    },
                    notify: false,
                    watchTask: true,
                    reloadOnRestart: true,
                    ghostMode: {
                        clicks: false,
                        forms: false,
                        scroll: false
                    }
                }
            }
        },

        // Compile sass
        sass: {
            dev: {
                options: {
                    style: 'expanded',
                    sourceComments: true,
                    sourceMap: true,
                },
                files: {
                    '<%= app.public %>/css/main.css': '<%= app.resources %>/styles/quickedit.scss',
                }
            },
            production: {
                options: {
                    style: 'compressed',
                    sourceComments: false,
                    sourceMap: false,
                },
                files: {
                    '<%= app.public %>/css/main.css': '<%= app.resources %>/styles/quickedit.scss',
                }
            },
        },

        // Concatenate scripts
        concat: {
            options: {},
            main: {
                src: [
                    '<%= app.resources %>/scripts/quickedit.js',
                ],
                dest: '<%= app.public %>/js/main.js',
            }
        },

        // Sync files
        sync: {
            fonts: {
                files: [{
                    cwd: '<%= app.resources %>/fonts/',
                    src: ['**/*.{eot,woff,woff2,ttf,svg}'],
                    dest: '<%= app.public %>/fonts'
                }],
                verbose: true,
                updateAndDelete: true,
            },
        },

        // Generate custom icon font
        webfont: {
            icons: {
                src: '<%= app.resources %>/icons/**/*.svg',
                dest: '<%= app.resources %>/fonts/',
                destCss: '<%= app.resources %>/styles/',
                options: {
                    font: 'quickedit-webfont',
                    engine: 'node',
                    types: 'eot,woff,woff2,ttf,svg',
                    hashes: false,
                    htmlDemo: false,
                    stylesheet: 'scss',
                    relativeFontPath: '#{$quickedit-font-path}',
                    templateOptions: {
                        baseClass: 'quickedit-icon',
                        classPrefix: 'quickedit-icon-',
                        mixinPrefix: 'quickedit-icon-'
                    },
                }
            }
        },
    });

    // dev task
    grunt.registerTask('dev', function(target) {
        grunt.task.run([
            'sass:dev',
            'concat',
            'sync',
        ]);
        if (target === 'watch') {
            return grunt.task.run(['browserSync', 'watch']);
        }
    });

    // default task
    grunt.registerTask('default', function() {
        grunt.task.run(['webfont']);
    });
};
