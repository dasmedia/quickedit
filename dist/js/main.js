/* jshint expr: true */

jQuery.fn.editorTabs = function() {
    jQuery(this).find('.editortoolbars div').hide(); //Hide all toolbars
    // jQuery(this).find('.editortab_content').hide(); //Hide all content
    // jQuery(this).find('ul.editor-tabs li:first').addClass('activeTab').show(); //Activate first tab
    jQuery(this).find('.editortoolbars div:first').show(); //Show first toolbar
    // jQuery(this).find('.editortab_content:first').show(); //Show first tab content
    jQuery('ul.editor-tabs li').click(function() {
        jQuery(this).parent().parent().find('ul.editor-tabs li').removeClass('activeTab'); //Remove any 'active' class
        jQuery(this).addClass('activeTab'); //Add 'active' class to selected tab
        jQuery(this).parent().parent().parent().find('.editortab_content').hide(); //Hide all tab content
        jQuery(this).parent().parent().parent().find('.editortoolbars div').hide(); //Hide all toolbars
        var activeTab = jQuery(this).find('a').attr('href'); //Find the rel attribute value to identify the active tab + content
        jQuery(activeTab).show(); //Fade in the active content
        var activeToolbar = activeTab + '_toolbar';
        jQuery(activeToolbar).show(); //Fade in the active toolbar
        return false;
    });
    if (document.location.hash !== '') {
        var tabSelect = document.location.hash.substr(1, document.location.hash.length);
        jQuery('a[href=#' + tabSelect + ']').parent('li').trigger('click');
        //        jQuery('html, body').animate({ scrollTop: $('li.activeTab').position().top }, 'slow');
    }
};


InitTranslationsOnlineEdit = function(config) {
    $(function() {

        var target = null;
        var inline_edit_icon_hide_timeout = 500;
        var inline_edit_icon_hide_timeout_id = 0;
        var $inline_edit_icon = $('<i class="c-quickedit__icon-pencil quickedit-icon quickedit-icon-pencil"></i>')
            .css({
                display: 'none',
            });

        $(document.body).append($inline_edit_icon);

        $inline_edit_icon.mouseover(function() {
            inline_edit_icon_hide_timeout_id && window.clearTimeout(inline_edit_icon_hide_timeout_id);
            $inline_edit_icon.show();
        });

        $inline_edit_icon.mouseout(function() {
            inline_edit_icon_hide_timeout_id && window.clearTimeout(inline_edit_icon_hide_timeout_id);
            inline_edit_icon_hide_timeout_id = setTimeout(function() {
                $inline_edit_icon.hide();
            }, inline_edit_icon_hide_timeout);
        });

        $('dfn.inline_edit').mouseover(function() {
            var offset = $(this).offset();
            target = this;
            inline_edit_icon_hide_timeout_id && window.clearTimeout(inline_edit_icon_hide_timeout_id);
            $inline_edit_icon.css({
                display: '',
                top: offset.top + 'px',
                left: (offset.left + $(this).width()) + 'px'
            });
        });

        $('dfn.inline_edit').mouseout(function() {
            inline_edit_icon_hide_timeout_id && window.clearTimeout(inline_edit_icon_hide_timeout_id);
            inline_edit_icon_hide_timeout_id = setTimeout(function() {
                $inline_edit_icon.hide();
            }, inline_edit_icon_hide_timeout);
        });

        var dialog = jQuery('<div class="inline_edit_dialog"></div>');
        var dialogoverlay = jQuery('<div class="inline_edit_dialog_overlay js-inline_edit_dialog_overlay"></div>');
        jQuery('body').append(dialogoverlay);
        jQuery('body').append(dialog);

        dialog.hide();
        dialogoverlay.hide();

        $inline_edit_icon.click(function() {
            var $target = $(target);
            var id = parseInt($(target).attr('rel') || 0);
            if (id) {
                var url = config.service_url + id;
                if ($(target).hasClass('editor')) {
                    url = config.editor_url + id;
                }

                $.get(url, {}, function(html) {
                    // var height = (jQuery(window).height() * 0.6);

                    dialog.html(html);

                    var btnSave = jQuery('<button class="button_save">' + config.translations.save + '</button>');
                    jQuery(dialog).append(btnSave);
                    btnSave.on('click', function(e) {
                        e.preventDefault();
                        var data_to_post = jQuery('#translationForm').serialize();
                        jQuery.post(url, data_to_post, function(data) {
                            jQuery('dfn.inline_edit[rel="' + id + '"]').each(function(i, obj) {
                                jQuery(obj).html(data.T9n.value || '');
                                if (data.T9n.id !== undefined) {
                                    jQuery(obj).attr('rel', data.T9n.id);
                                }
                            });
                            dialog.hide();
                            dialogoverlay.hide();
                        }, 'json');
                    });

                    var btnCancel = jQuery('<button class="button_cancel">' + config.translations.cancel + '</button>');
                    jQuery(dialog).append(btnCancel);
                    btnCancel.on('click', function(e) {
                        e.preventDefault();
                        dialog.hide();
                        dialogoverlay.hide();
                    });
                    jQuery(document).keyup(function(e) {
                        if (e.keyCode == 27) {
                            dialog.hide();
                            dialogoverlay.hide();
                        }
                    });
                    jQuery('.js-inline_edit_dialog_overlay').on('click', function(e) {
                        e.preventDefault();
                        dialog.hide();
                        dialogoverlay.hide();
                    });

                    dialog.show();
                    dialogoverlay.show();
                    jQuery("div[class^='widget']", dialog).editorTabs(); //Run function on any div with class name of "editortab_content"
                });
            }
        });
    });
};
